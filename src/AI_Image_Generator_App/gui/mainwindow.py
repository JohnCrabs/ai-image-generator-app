import tkinter as tk
from tkinter import ttk
from tkinter import filedialog

import os

from PIL import Image, ImageTk, ImageOps
from diffusers.utils import load_image
from src.AI_Image_Generator_App.lib.StableDiffusionApplications import StableDiffusionApplications

FONT_LABEL_TAB = "Helvetica 10 bold"
FONT_LABEL_FRAME = "Helvetica 9 bold"
FONT_TEXT = "Helvetica 9"

TAB_T2I = "Text to Image"
TAB_I2I = "Image to Image"
TAB_InI = "Inpainting Image"


class MainWindow:
    def __init__(self):
        # Initialize generative models
        self.sda = StableDiffusionApplications()

        # Create the main window
        self.rWin = tk.Tk()
        self.setRootWin(
            title="Image Generator",
            resizable=True,
            iconPath=""
        )
        self.rWin.wm_minsize(720, 524)
        self.rWin.geometry("1024x524")
        # ------ START BUILDING THE APP ------
        # Common Variables
        self.tvar_ExportDir = tk.StringVar()
        self.tvar_ExportImageName = tk.StringVar()
        self.tvar_ExportImageName.set("img.png")

        self.tvar_t2i_curr_index = tk.StringVar()
        self.tvar_i2i_curr_index = tk.StringVar()
        self.tvar_ini_curr_index = tk.StringVar()

        self.tvar_t2i_max_index = tk.StringVar()
        self.tvar_i2i_max_index = tk.StringVar()
        self.tvar_ini_max_index = tk.StringVar()

        self.tvar_t2i_ImageNumGen = tk.StringVar()
        self.tvar_i2i_ImageNumGen = tk.StringVar()
        self.tvar_ini_ImageNumGen = tk.StringVar()

        self.tvar_i2i_imgPath = tk.StringVar()
        self.img_i2i_input = None
        self.imgTk_i2i_input = None
        self.tvar_i2i_maxAspectSize = tk.StringVar()

        self.tvar_ini_imgPath = tk.StringVar()
        self.img_ini_input = None
        self.imgTk_ini_input = None

        self.tvar_ini_imgPath_mask = tk.StringVar()
        self.img_ini_input_mask = None
        self.imgTk_ini_input_mask = None

        self.tvar_ini_maxAspectSize = tk.StringVar()

        self.img = {
            TAB_T2I: [],
            TAB_I2I: [],
            TAB_InI: []
        }

        self.img_index = {
            TAB_T2I: 0,
            TAB_I2I: 0,
            TAB_InI: 0
        }

        self.curr_t2i_img = None
        self.curr_i2i_img = None
        self.curr_ini_img = None

        # =====================================================================================================
        # Set Export Path
        # =====================================================================================================
        # START ==> LabelFrame ---> ExportImage
        lf_ExportImage = tk.LabelFrame(self.rWin, text="Export Image", font=FONT_LABEL_FRAME)

        # START ==> Frame ---> ExportPath
        f_ExportPath = tk.Frame(lf_ExportImage)
        tk.Label(f_ExportPath, text="Folder Path: ", font=FONT_TEXT).pack(side="left", padx=2)
        tk.Entry(f_ExportPath, textvariable=self.tvar_ExportDir, state="readonly").pack(side="left", expand=True,
                                                                                        fill="x",
                                                                                        padx=2)
        tk.Button(f_ExportPath, text="Browse", command=self.browse_Dir).pack(side="left", padx=2, pady=5)
        f_ExportPath.pack(anchor="n", fill="x")
        # END ==> Frame ---> ExportPath

        # START ==> Frame ---> ExportImageInfo
        f_ExportImageInfo = tk.Frame(lf_ExportImage)
        tk.Label(f_ExportImageInfo, text="Image Name: ", font=FONT_TEXT).pack(side="left", padx=2)
        tk.Entry(f_ExportImageInfo, textvariable=self.tvar_ExportImageName, justify=tk.CENTER).pack(side="left", padx=2)
        tk.Button(f_ExportImageInfo, text="Save", command=self.save_Image).pack(side="left", padx=2)
        f_ExportImageInfo.pack(anchor="n", fill="x")
        # END ==> Frame ---> ExportImageInfo

        lf_ExportImage.pack(anchor="n", expand=False, fill="x", ipadx=2, ipady=2, padx=2, pady=2)
        # END ==> LabelFrame ---> ExportImage

        # =====================================================================================================
        # Set Tab Menu
        # =====================================================================================================
        lf_TabMenu = tk.LabelFrame(self.rWin, text="Generative Methods", font=FONT_LABEL_FRAME)
        s = ttk.Style()
        s.configure('TNotebook.Tab', font=FONT_LABEL_TAB)

        self.tabControl = ttk.Notebook(lf_TabMenu)

        tab_Text2Image = ttk.Frame(self.tabControl)
        tab_Image2Image = ttk.Frame(self.tabControl)
        tab_InpaintImage = ttk.Frame(self.tabControl)

        self.tabControl.add(tab_Text2Image, text=TAB_T2I, padding=2)
        self.tabControl.add(tab_Image2Image, text=TAB_I2I, padding=2)
        self.tabControl.add(tab_InpaintImage, text=TAB_InI, padding=2)

        self.tabControl.pack(expand=True, fill="both", padx=2, pady=2)
        lf_TabMenu.pack(anchor="n", expand=True, fill="both", ipadx=2, ipady=2, padx=2, pady=2)

        # ***********************************************************
        # Tab Text2Image
        # ***********************************************************
        # Set Tab Text2Image
        lf_Text2ImageInput = tk.LabelFrame(tab_Text2Image, text="Input", font=FONT_LABEL_FRAME)
        tk.Label(lf_Text2ImageInput, text="Text Prompt: ", font=FONT_TEXT).grid(row=0, column=0, padx=2, pady=5)
        self.tvar_t2i_TextPrompt = tk.Text(lf_Text2ImageInput, height=8)
        self.tvar_t2i_TextPrompt.grid(row=1, column=0)
        tk.Label(lf_Text2ImageInput, text="Negative Prompt: ", font=FONT_TEXT).grid(row=2, column=0, padx=2, pady=5)
        self.tvar_t2i_NegativePrompt = tk.Text(lf_Text2ImageInput, height=8)
        self.tvar_t2i_NegativePrompt.grid(row=3, column=0)

        f_ImageNumber = tk.Frame(lf_Text2ImageInput)
        tk.Label(f_ImageNumber, text="Generate Images: ", font=FONT_TEXT).grid(row=0, column=0)
        tk.Spinbox(f_ImageNumber, textvariable=self.tvar_t2i_ImageNumGen, state="readonly", justify="center",
                   values=[str(x) for x in range(1, 100)], width=5).grid(row=0, column=1)
        tk.Button(f_ImageNumber, text="Generate", command=self.generate_Text2Image).grid(row=0, column=2, padx=2)
        tk.Button(f_ImageNumber, text="Reset", command=self.reset_Text2Image).grid(row=0, column=3, padx=2)
        f_ImageNumber.grid(row=4, column=0, sticky="w", padx=2, pady=5)
        lf_Text2ImageInput.pack(side="left", padx=2, fill="y")

        lf_Text2ImageCanvas = tk.LabelFrame(tab_Text2Image, text="Preview Image", font=FONT_LABEL_FRAME)

        # START ==> Frame ---> ChangeImage
        f_ChangeImage_text2img = tk.Frame(lf_Text2ImageCanvas)
        tk.Button(f_ChangeImage_text2img, text="<<-<<-", command=self.previous_img).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_text2img, textvariable=self.tvar_t2i_curr_index, font=FONT_TEXT).pack(side="left",
                                                                                                     padx=2)
        tk.Label(f_ChangeImage_text2img, text="  /  ", font=FONT_TEXT).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_text2img, textvariable=self.tvar_t2i_max_index, font=FONT_TEXT).pack(side="left", padx=2)
        tk.Button(f_ChangeImage_text2img, text="->>->>", command=self.next_img).pack(side="left", padx=2)
        f_ChangeImage_text2img.pack(anchor="n", fill="x")
        # END ==> Frame ---> ChangeImage

        self.canvas_t2i_img = tk.Canvas(lf_Text2ImageCanvas, bg="black")
        self.canvas_t2i_img.pack(anchor="n", expand=True, fill="both")

        lf_Text2ImageCanvas.pack(side="left", padx=2, expand=True, fill="both")

        self.reset_Text2Image()

        # ***********************************************************
        # Tab Image2Image
        # ***********************************************************

        # Add input image frame
        lf_Image2ImageInput = tk.LabelFrame(tab_Image2Image, text="Input", font=FONT_LABEL_FRAME)

        f_InputToolbar_i2i = tk.Frame(lf_Image2ImageInput)
        tk.Button(f_InputToolbar_i2i, text="Open Image", command=self.openImg_Image2Image).grid(row=0, column=0, padx=2)
        tk.Label(f_InputToolbar_i2i, text="Generate Images: ", font=FONT_TEXT).grid(row=0, column=1, padx=2)
        tk.Spinbox(f_InputToolbar_i2i, textvariable=self.tvar_i2i_ImageNumGen, state="readonly", justify="center",
                   values=[str(x) for x in range(1, 100)], width=5).grid(row=0, column=2, padx=2)
        tk.Label(f_InputToolbar_i2i, text="Max Aspect Size: ", font=FONT_TEXT).grid(row=0, column=3, padx=2)
        tk.Spinbox(f_InputToolbar_i2i, textvariable=self.tvar_i2i_maxAspectSize, justify="center",
                   values=[str(x) for x in range(32, 4096)], width=5).grid(row=0, column=4, padx=2)
        self.tvar_i2i_maxAspectSize.set("256")
        tk.Button(f_InputToolbar_i2i, text="Generate", command=self.generate_Image2Image).grid(row=0, column=5, padx=2)
        f_InputToolbar_i2i.pack(anchor='n', fill='x', padx=2)

        f_Canvas_Prompts = tk.Frame(lf_Image2ImageInput)
        f_i2i_Canvas_Input = tk.Frame(f_Canvas_Prompts)
        self.canvas_i2i_input_img = tk.Canvas(f_i2i_Canvas_Input, bg="black", width=240, height=240)
        self.canvas_i2i_input_img.pack(anchor="n", pady=50)
        f_i2i_Canvas_Input.pack(side="left", anchor="n", expand=True, fill="both", padx=2)

        f_i2i_Prompts = tk.Frame(f_Canvas_Prompts)
        tk.Label(f_i2i_Prompts, text="Text Prompt: ", font=FONT_TEXT).grid(row=0, column=0, padx=2, pady=5)
        self.tvar_i2i_TextPrompt = tk.Text(f_i2i_Prompts, height=8, width=40)
        self.tvar_i2i_TextPrompt.grid(row=1, column=0)
        tk.Label(f_i2i_Prompts, text="Negative Prompt: ", font=FONT_TEXT).grid(row=2, column=0, padx=2, pady=5)
        self.tvar_i2i_NegativePrompt = tk.Text(f_i2i_Prompts, height=8, width=40)
        self.tvar_i2i_NegativePrompt.grid(row=3, column=0)
        f_i2i_Prompts.pack(side="left", anchor="n", expand=True, fill="both", padx=2)

        f_Canvas_Prompts.pack(anchor="n", expand=True, fill="both")

        lf_Image2ImageInput.pack(side="left", padx=2, expand=True, fill="both")

        # Add output image frame
        lf_Image2ImageCanvas = tk.LabelFrame(tab_Image2Image, text="Preview Image", font=FONT_LABEL_FRAME)
        # START ==> Frame ---> ChangeImage
        f_ChangeImage_img2img = tk.Frame(lf_Image2ImageCanvas)
        tk.Button(f_ChangeImage_img2img, text="<<-<<-", command=self.previous_img).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_img2img, textvariable=self.tvar_i2i_curr_index, font=FONT_TEXT).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_img2img, text="  /  ", font=FONT_TEXT).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_img2img, textvariable=self.tvar_i2i_max_index, font=FONT_TEXT).pack(side="left", padx=2)
        tk.Button(f_ChangeImage_img2img, text="->>->>", command=self.next_img).pack(side="left", padx=2)
        f_ChangeImage_img2img.pack(anchor="n", fill="x")
        # END ==> Frame ---> ChangeImage

        self.canvas_i2i_img = tk.Canvas(lf_Image2ImageCanvas, bg="black")
        self.canvas_i2i_img.pack(anchor="n", expand=True, fill="both")

        lf_Image2ImageCanvas.pack(side="left", padx=2, expand=True, fill="both")

        # ***********************************************************
        # Tab InpaintImage
        # ***********************************************************
        lf_InpaintImageInput = tk.LabelFrame(tab_InpaintImage, text="Input", font=FONT_LABEL_FRAME)

        f_InputCanvas_inpaint = tk.Frame(lf_InpaintImageInput)
        f_InputImage_inpaint = tk.Frame(f_InputCanvas_inpaint)
        tk.Button(f_InputImage_inpaint, text="Open Input Image", command=self.openImg_InpaintBaseImage).pack(anchor='n')
        self.canvas_inpaint_input_img = tk.Canvas(f_InputImage_inpaint, bg="black", width=148, height=148)
        self.canvas_inpaint_input_img.pack(anchor='n')
        f_InputImage_inpaint.pack(anchor='n')

        f_InputMask_inpaint = tk.Frame(f_InputCanvas_inpaint)
        tk.Button(f_InputMask_inpaint, text="Open Mask Image", command=self.openImg_InpaintMaskImage).pack(anchor='n')
        self.canvas_inpaint_input_mask = tk.Canvas(f_InputMask_inpaint, bg="black", width=148, height=148)
        self.canvas_inpaint_input_mask.pack(anchor='n')
        f_InputMask_inpaint.pack(anchor='n')
        f_InputCanvas_inpaint.pack(side="left", anchor='n', padx=2)

        f_input_inpaint_prompts = tk.Frame(lf_InpaintImageInput)

        f_InputToolbar_ini = tk.Frame(f_input_inpaint_prompts)
        tk.Label(f_InputToolbar_ini, text="Generate Images: ", font=FONT_TEXT).grid(row=0, column=1, padx=2)
        tk.Spinbox(f_InputToolbar_ini, textvariable=self.tvar_ini_ImageNumGen, state="readonly", justify="center",
                   values=[str(x) for x in range(1, 100)], width=5).grid(row=0, column=2, padx=2)
        tk.Label(f_InputToolbar_ini, text="Max Aspect Size: ", font=FONT_TEXT).grid(row=0, column=3, padx=2)
        tk.Spinbox(f_InputToolbar_ini, textvariable=self.tvar_ini_maxAspectSize, justify="center",
                   values=[str(x) for x in range(32, 4096)], width=5).grid(row=0, column=4, padx=2)
        self.tvar_ini_maxAspectSize.set("256")
        tk.Button(f_InputToolbar_ini, text="Generate", command=self.generate_Inpainting).grid(row=0, column=5, padx=2)
        f_InputToolbar_ini.pack(anchor='n', fill='x', padx=2)

        f_inpaint_Prompts = tk.Frame(f_input_inpaint_prompts)
        tk.Label(f_inpaint_Prompts, text="Text Prompt: ", font=FONT_TEXT).grid(row=0, column=0, padx=2, pady=5)
        self.tvar_inpaint_TextPrompt = tk.Text(f_inpaint_Prompts, height=8, width=60)
        self.tvar_inpaint_TextPrompt.grid(row=1, column=0)
        tk.Label(f_inpaint_Prompts, text="Negative Prompt: ", font=FONT_TEXT).grid(row=2, column=0, padx=2, pady=5)
        self.tvar_inpaint_NegativePrompt = tk.Text(f_inpaint_Prompts, height=8, width=60)
        self.tvar_inpaint_NegativePrompt.grid(row=3, column=0)
        f_inpaint_Prompts.pack(anchor="n", expand=True, fill="both", padx=2)

        f_input_inpaint_prompts.pack(side="left", anchor="n", expand=True, fill="both", padx=2)
        lf_InpaintImageInput.pack(side="left", padx=2, fill="y")

        # Add output image frame
        lf_InpaintCanvas = tk.LabelFrame(tab_InpaintImage, text="Preview Image", font=FONT_LABEL_FRAME)
        # START ==> Frame ---> ChangeImage
        f_ChangeImage_inpaint = tk.Frame(lf_InpaintCanvas)
        tk.Button(f_ChangeImage_inpaint, text="<<-<<-", command=self.previous_img).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_inpaint, textvariable=self.tvar_ini_curr_index, font=FONT_TEXT).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_inpaint, text="  /  ", font=FONT_TEXT).pack(side="left", padx=2)
        tk.Label(f_ChangeImage_inpaint, textvariable=self.tvar_ini_max_index, font=FONT_TEXT).pack(side="left", padx=2)
        tk.Button(f_ChangeImage_inpaint, text="->>->>", command=self.next_img).pack(side="left", padx=2)
        f_ChangeImage_inpaint.pack(anchor="n", fill="x")
        # END ==> Frame ---> ChangeImage

        self.canvas_ini_img = tk.Canvas(lf_InpaintCanvas, bg="black")
        self.canvas_ini_img.pack(anchor="n", expand=True, fill="both")

        lf_InpaintCanvas.pack(side="left", padx=2, expand=True, fill="both")

        # ------ END OF CODE SECTION / START EXEC LOOP ------
        # Bind Events
        self.rWin.bind("<Configure>", self.on_window_resize)
        # The MainLoop
        self.rWin.mainloop()

    @staticmethod
    def browseFile(filetypes=()):
        pathFile = filedialog.askopenfilename(
            filetypes=filetypes
        )
        if os.path.exists(pathFile):
            return pathFile
        return None

    def on_window_resize(self, *args):
        self.configure_canvas_t2i()
        self.configure_canvas_i2i()
        self.configure_canvas_ini()

    def configure_canvas_t2i(self):
        currTab = self.getCurrTab()
        if currTab == TAB_T2I:
            if len(self.img[currTab]) > 0:
                try:
                    img = self.img[currTab][self.img_index[currTab]]
                    new_size = (self.canvas_t2i_img.winfo_width(), self.canvas_t2i_img.winfo_height())
                    img.thumbnail(new_size, Image.LANCZOS)
                    self.curr_t2i_img = ImageTk.PhotoImage(img)
                    w, h = img.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_t2i_img.delete('all')
                    self.canvas_t2i_img.create_image(screen_w, screen_h, anchor=tk.NW, image=self.curr_t2i_img)
                    self.tvar_t2i_curr_index.set(str(self.img_index[currTab] + 1))
                except:
                    print("Error: Output Image for <configure_canvas_t2i> is not of type PIL")

    def configure_canvas_i2i(self):
        currTab = self.getCurrTab()
        if currTab == TAB_I2I:
            if self.img_i2i_input:
                try:
                    inputImg = self.img_i2i_input.copy()
                    new_size = (self.canvas_i2i_input_img.winfo_width(), self.canvas_i2i_input_img.winfo_height())
                    inputImg.thumbnail(new_size, Image.LANCZOS)
                    self.imgTk_i2i_input = ImageTk.PhotoImage(inputImg)
                    w, h = inputImg.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_i2i_input_img.delete('all')
                    self.canvas_i2i_input_img.create_image(screen_w, screen_h, anchor=tk.NW, image=self.imgTk_i2i_input)
                except:
                    print("Error: Input Image for <configure_canvas_i2i> is not of type PIL")

            if len(self.img[currTab]) > 0:
                try:
                    img = self.img[currTab][self.img_index[currTab]]
                    new_size = (self.canvas_i2i_img.winfo_width(), self.canvas_i2i_img.winfo_height())
                    img.thumbnail(new_size, Image.LANCZOS)
                    self.curr_i2i_img = ImageTk.PhotoImage(img)
                    w, h = img.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_i2i_img.delete('all')
                    self.canvas_i2i_img.create_image(screen_w, screen_h, anchor=tk.NW, image=self.curr_i2i_img)
                    self.tvar_i2i_curr_index.set(str(self.img_index[currTab] + 1))
                except:
                    print("Error: Output Image for <configure_canvas_i2i> is not of type PIL")

    def configure_canvas_ini(self):
        currTab = self.getCurrTab()
        if currTab == TAB_InI:
            if self.img_ini_input:
                try:
                    inputImg = self.img_ini_input.copy()
                    new_size = (self.canvas_inpaint_input_img.winfo_width(), self.canvas_inpaint_input_img.winfo_height())
                    inputImg.thumbnail(new_size, Image.LANCZOS)
                    self.imgTk_ini_input = ImageTk.PhotoImage(inputImg)
                    w, h = inputImg.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_inpaint_input_img.delete('all')
                    self.canvas_inpaint_input_img.create_image(screen_w, screen_h, anchor=tk.NW, image=self.imgTk_ini_input)
                except:
                    print("Error: Input Image for <configure_canvas_i2i> is not of type PIL")

            if self.img_ini_input_mask:
                try:
                    inputImg = self.img_ini_input_mask.copy()
                    new_size = (self.canvas_inpaint_input_mask.winfo_width(), self.canvas_inpaint_input_mask.winfo_height())
                    inputImg.thumbnail(new_size, Image.LANCZOS)
                    self.imgTk_ini_input_mask = ImageTk.PhotoImage(inputImg)
                    w, h = inputImg.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_inpaint_input_mask.delete('all')
                    self.canvas_inpaint_input_mask.create_image(screen_w, screen_h, anchor=tk.NW, image=self.imgTk_ini_input_mask)
                except:
                    print("Error: Input Image for <configure_canvas_i2i> is not of type PIL")

            if len(self.img[currTab]) > 0:
                try:
                    img = self.img[currTab][self.img_index[currTab]]
                    new_size = (self.canvas_ini_img.winfo_width(), self.canvas_ini_img.winfo_height())
                    img.thumbnail(new_size, Image.LANCZOS)
                    self.curr_ini_img = ImageTk.PhotoImage(img)
                    w, h = img.size
                    screen_w = new_size[0] / 2 - w / 2
                    screen_h = new_size[1] / 2 - h / 2
                    self.canvas_ini_img.delete('all')
                    self.canvas_ini_img.create_image(screen_w, screen_h, anchor=tk.NW, image=self.curr_ini_img)
                    self.tvar_ini_curr_index.set(str(self.img_index[currTab] + 1))
                except:
                    print("Error: Output Image for <configure_canvas_i2i> is not of type PIL")

    def getCurrTab(self):
        return self.tabControl.tab(self.tabControl.select(), "text")

    def previous_img(self):
        currTab = self.getCurrTab()
        genSize = len(self.img[currTab])
        if genSize > 0:
            if self.img_index[currTab] - 1 >= 0:
                self.img_index[currTab] -= 1
            else:
                self.img_index[currTab] = genSize - 1
        self.on_window_resize()

    def next_img(self):
        currTab = self.getCurrTab()
        genSize = len(self.img[currTab])
        if genSize > 0:
            if self.img_index[currTab] + 1 < genSize:
                self.img_index[currTab] += 1
            else:
                self.img_index[currTab] = 0
        self.on_window_resize()

    def setRootWin(self, title: str, resizable: bool, iconPath: str):
        self.rWin.title(title)
        try:
            self.rWin.iconphoto(False, tk.PhotoImage(file=iconPath))
        except:
            pass
        self.rWin.resizable(resizable, resizable)

    def browse_Dir(self):
        pathDir = filedialog.askdirectory()
        if os.path.exists(pathDir):
            self.tvar_ExportDir.set(pathDir)

    def save_Image(self):
        currTabSelected = self.getCurrTab()
        exportPath = os.path.normpath(self.tvar_ExportDir.get() + '/' + self.tvar_ExportImageName.get())
        img = self.img[currTabSelected][self.img_index[currTabSelected]]
        if type(img) is Image.Image:
            img.save(exportPath)

    def generate_Text2Image(self):
        text_prompt = self.tvar_t2i_TextPrompt.get("1.0", "end")
        negative_prompt = self.tvar_t2i_NegativePrompt.get("1.0", "end")
        for _ in range(int(self.tvar_t2i_ImageNumGen.get())):
            img = self.sda.text2image(prompt=text_prompt,
                                      negative_prompt=negative_prompt,
                                      height=480,
                                      width=480)
            self.img[TAB_T2I].append(img)
            self.img_index[TAB_T2I] = len(self.img[TAB_T2I]) - 1
            self.tvar_t2i_max_index.set(str(len(self.img[TAB_T2I])))
        self.configure_canvas_t2i()

    def reset_Text2Image(self):
        self.tvar_t2i_TextPrompt.delete("1.0", "end")
        self.tvar_t2i_NegativePrompt.delete("1.0", "end")
        self.tvar_t2i_ImageNumGen.set("1")
        self.canvas_t2i_img.delete('all')
        self.img[TAB_T2I] = []
        self.img_index[TAB_T2I] = 0
        self.tvar_t2i_curr_index.set(str(self.img_index[TAB_T2I]))
        self.tvar_t2i_max_index.set(str(self.img_index[TAB_T2I]))

    def openImg_Image2Image(self):
        imgPath = self.browseFile(
            filetypes=(
                ("Image File", ("*.png", "*.jpg", "*.jpeg", "*.tiff")),
                ("All Files", "*.*"),
            )
        )
        if imgPath:
            self.tvar_i2i_imgPath.set(imgPath)
            self.img_i2i_input = Image.open(self.tvar_i2i_imgPath.get())
            self.configure_canvas_i2i()

    def generate_Image2Image(self):
        text_prompt = self.tvar_i2i_TextPrompt.get("1.0", "end")
        negative_prompt = self.tvar_i2i_NegativePrompt.get("1.0", "end")
        img_input = load_image(self.tvar_i2i_imgPath.get())
        img_in_size = list(img_input.size)
        img_max_size = max(img_in_size)
        maxAspectSize = int(self.tvar_i2i_maxAspectSize.get())
        if img_max_size > maxAspectSize:
            divider = int(img_max_size/maxAspectSize)
            img_in_size[0] = int(img_in_size[0] / divider)
            img_in_size[1] = int(img_in_size[1] / divider)
            img_input.thumbnail(tuple(img_in_size))
        if img_input:
            for _ in range(int(self.tvar_i2i_ImageNumGen.get())):
                img = self.sda.img2img(img=img_input,
                                       prompt=text_prompt,
                                       n_prompt=negative_prompt
                                       )
                self.img[TAB_I2I].append(img)
                self.img_index[TAB_I2I] = len(self.img[TAB_I2I]) - 1
                self.tvar_i2i_max_index.set(str(len(self.img[TAB_I2I])))
            self.configure_canvas_i2i()

    def openImg_InpaintBaseImage(self):
        imgPath = self.browseFile(
            filetypes=(
                ("Image File", ("*.png", "*.jpg", "*.jpeg", "*.tiff")),
                ("All Files", "*.*"),
            )
        )
        if imgPath:
            self.tvar_ini_imgPath.set(imgPath)
            self.img_ini_input = Image.open(self.tvar_ini_imgPath.get())
            self.configure_canvas_ini()

    def openImg_InpaintMaskImage(self):
        imgPath = self.browseFile(
            filetypes=(
                ("Image File", ("*.png", "*.jpg", "*.jpeg", "*.tiff")),
                ("All Files", "*.*"),
            )
        )
        if imgPath:
            self.tvar_ini_imgPath_mask.set(imgPath)
            self.img_ini_input_mask = Image.open(self.tvar_ini_imgPath_mask.get())
            self.configure_canvas_ini()

    def generate_Inpainting(self):
        text_prompt = self.tvar_inpaint_TextPrompt.get("1.0", "end")
        negative_prompt = self.tvar_inpaint_NegativePrompt.get("1.0", "end")
        img_input = load_image(self.tvar_ini_imgPath.get())
        img_input_mask = load_image(self.tvar_ini_imgPath_mask.get())

        if img_input and img_input_mask:
            img_in_size: list[int] = list(img_input.size)
            img_max_size = max(img_in_size)
            maxAspectSize = int(self.tvar_ini_maxAspectSize.get())

            if img_max_size > maxAspectSize:
                divider = int(img_max_size / maxAspectSize)
                img_in_size[0] = int(img_in_size[0] / divider)
                img_in_size[1] = int(img_in_size[1] / divider)
                img_input.thumbnail(tuple(img_in_size))
                img_input_mask.thumbnail(tuple(img_in_size))
            else:
                img_input_mask.thumbnail(tuple(img_in_size))

            for _ in range(int(self.tvar_ini_ImageNumGen.get())):
                img = self.sda.image_inpainting(img=img_input,
                                                mask=img_input_mask,
                                                prompt=text_prompt,
                                                n_prompt=negative_prompt
                                                )
                self.img[TAB_InI].append(img)
                self.img_index[TAB_InI] = len(self.img[TAB_InI]) - 1
                self.tvar_ini_max_index.set(str(len(self.img[TAB_InI])))
                self.configure_canvas_ini()


if __name__ == "__main__":
    win = MainWindow()
