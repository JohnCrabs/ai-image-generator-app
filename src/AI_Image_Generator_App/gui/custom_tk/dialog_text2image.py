import customtkinter as ctk
import xarray as xr


class Text2Image:
    def __init__(self, root=None,
                 i_width: int = 1280, i_height: int = 720,
                 b_resize_w: bool = True, b_resize_h: bool = True):
        b_open_as_mainwindow = False
        # ------ CREATE MAIN WINDOW IF NECESSARY ------
        self.root = root
        if self.root is None:
            self.root = ctk.CTk()  # Declare a new window object
            self.root.geometry(str(int(i_width)) + "x" + str(int(i_height)))  # Set the starting geometry
            self.root.resizable(b_resize_w, b_resize_h)  # Set if window is can be resized
            b_open_as_mainwindow = True  # Set the state to True for executing the mainloop()

        # ------ START BUILDING THE APP ------
        self.mainFrame = ctk.CTkFrame(self.root)

        self.mainFrame.pack()
        # ------ END OF BUILDING SECTION / START EXEC LOOP ------
        if b_open_as_mainwindow:
            self.root.mainloop()


if __name__ == "__main__":
    Text2Image(i_width=1024, i_height=512,
               b_resize_w=True, b_resize_h=True)
