import os
import torch
import numpy as np

from PIL import Image
from diffusers import (
    DiffusionPipeline,
    StableDiffusionXLImg2ImgPipeline,
    AutoPipelineForImage2Image,
    StableDiffusionInpaintPipeline
)


IMG_2_IMG_MODE_AUTO = 'auto'
IMG_2_IMG_MODE_XL = 'XLImg2Img'


class StableDiffusionApplications:
    def __init__(self, model_id: str = None, device: str = "cuda", use_safetensors=False):
        self.model_id = model_id
        self.device = "cpu" if device != "cuda" else ("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Device: {self.device}")
        self.generator = torch.Generator(self.device)
        self.use_safetensors = use_safetensors

        self._gen_RandomImage = self.compile_RandomImage()
        self._gen_text2img = self.compile_text2image()
        self._gen_img2img = self.compile_img2img()
        self._gen_Inpaint = self.compile_inpaint()

    @staticmethod
    def init_image(path: str, img_size: tuple[int, int] = (512, 512)):
        img = Image.open(path).convert("RGB")
        img.resize(img_size)
        return img

    def set_model_id(self, new_model_id: str):
        self.model_id = new_model_id

    def set_device(self, new_device: str):
        self.device = "cpu" if new_device != "cuda" else ("cuda" if torch.cuda.is_available() else "cpu")
        self.generator = torch.Generator(self.device)

    def compile_RandomImage(self):
        pipeline = DiffusionPipeline.from_pretrained(
            "anton-l/ddpm-butterflies-128",
            use_safetensors=self.use_safetensors
        )
        return pipeline.to(self.device)

    def compile_text2image(self):
        pipeline = DiffusionPipeline.from_pretrained(
            "runwayml/stable-diffusion-v1-5",
            torch_dtype=torch.float16,
            use_safetensors=self.use_safetensors
        )
        return pipeline.to(self.device)

    def compile_img2img(self, mode=IMG_2_IMG_MODE_AUTO):
        if mode == IMG_2_IMG_MODE_XL:
            pipeline = StableDiffusionXLImg2ImgPipeline.from_pretrained(
                "stabilityai/stable-diffusion-xl-refiner-1.0",
                torch_dtype=torch.float16,
                safety_checker=None
            )
        else:
            pipeline = AutoPipelineForImage2Image.from_pretrained(
                "kandinsky-community/kandinsky-2-2-decoder",
                torch_dtype=torch.float16,
                use_safetensors=True
            )
        pipeline.to(self.device)

        return pipeline

    def compile_inpaint(self):
        # "andregn/Realistic_Vision_V3.0-inpainting"
        pipeline = StableDiffusionInpaintPipeline.from_pretrained(
            "runwayml/stable-diffusion-inpainting",
            torch_dtype=torch.float16,
            use_safetensors=self.use_safetensors,
            variant="fp16",
        )
        return pipeline.to(self.device)

    def randomImage(self):
        print("Randomized Image:")
        return self._gen_RandomImage().images[0]

    def text2image(self, prompt: str, negative_prompt: str = "", height=512, width=512):
        print("Text to Image:")
        return self._gen_text2img(prompt=prompt, negative_prompt=negative_prompt,
                                  height=height, width=width,
                                  generator=self.generator).images[0]

    def img2img(self, img, prompt: str or list[str] or None = None, n_prompt: str or list[str] or None = None):
        print("Image to Image:")
        return self._gen_img2img(image=img, prompt=prompt, negative_prompt=n_prompt).images[0]

    def image_inpainting(self, img, mask, prompt: str or list[str] or None = None, n_prompt: str or list[str] or None = None):
        print("Image Inpainting:")
        return self._gen_Inpaint(prompt=prompt, negative_prompt=n_prompt, image=img, mask_image=mask).images[0]

    def mass_randomImage(self):
        pass

    def mass_text2image(self):
        pass

    def mass_img2img(self):
        pass

    def mass_image_inpainting(self, img, mask, iters: int, export_to: str, prompt: str or list[str] or None = None):
        if not os.path.exists(export_to):
            os.makedirs(export_to)
        num_zeros = str(iters).__len__()
        for __index__ in range(iters):
            fileName = str(__index__).zfill(num_zeros) + ".png"
            print("Generating image: " + fileName)
            img = self.image_inpainting(
                img=img,
                mask=mask,
                prompt=prompt
            )
            export_path = os.path.normpath(export_to + "/" + fileName)
            print("Export image to: " + export_path)
            img.save(export_path)


if __name__ == "__main__":
    # Create application core
    sda = StableDiffusionApplications()
    # Text to Image
    text_prompt = "a realistic design background game, HD, 4K"
    image = sda.text2image(text_prompt)

