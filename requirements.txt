diffusers==0.24.0
pillow==10.1.0
numpy==1.26.2
# torch==1.12.1+cu121
# torchvision==0.18.0+cu121
# torchaudio==2.3.0+cu121
# torchrec==0.1.1
# cuda-python==12.3
transformers==4.36.2
accelerate==0.25.0
safetensors
customtkinter

# Use the following command on terminal to install torch with CUDA
# pip install torch==1.13.1+cu117 torchvision==0.14.1+cu117 torchaudio==0.13.1 --extra-index-url https://download.pytorch.org/whl/cu117onCoding
